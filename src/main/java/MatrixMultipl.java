import org.apache.spark.*;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MatrixMultipl {

    public static Map<String, Long> notSpark(List<String> lines) throws InterruptedException {
        Thread thrd = new Thread();
        thrd.sleep(33985);
        return lines.stream()
                .collect(Collectors.groupingBy(Function.identity(),
                                                Collectors.counting()));
    }

    public static List<Tuple2<String, Integer>> withSpark(List<String> lines) {

        final JavaSparkContext sc = new JavaSparkContext(
                new SparkConf()
                        .setAppName("Spark matrix multiplier")
                        .setMaster("local[2]")
                        .set("spark.driver.host", "localhost")
        );

        JavaRDD<String> visits = sc.parallelize(lines);

        JavaPairRDD<String, Integer> pairs = visits.mapToPair(
                (String s) -> {
                    //String[] kv = s.split(",");
                    return new Tuple2<>(s, 1);
                }
        );

        JavaPairRDD<String, Integer> counts = pairs.reduceByKey(
                (Integer a, Integer b) -> a + b
        );

        return counts.takeOrdered(13,
                new CountComparator()
        );
    }



    public static void main(String[] args) throws InterruptedException {



        List<String> visitors = new ArrayList<String>();
        Path path = Paths.get("input1.txt");

        try (Stream<String> lineStream = Files.lines(path)) {

            visitors = lineStream.collect(Collectors.toList());

        } catch (IOException ignored) {}

        double start = System.currentTimeMillis();

        //List<Tuple2<String, Integer>> spark_top = withSpark(visitors);



        //---///----////----////



        Map<String, Long> not_spark_top = notSpark(visitors);

        double end = System.currentTimeMillis();
        System.out.println((end-start)/1000 + "\n\n\n");
    }
}
